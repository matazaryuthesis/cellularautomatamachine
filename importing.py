import os
import importlib.util

print(os.listdir("lattice"))

spec = importlib.util.spec_from_file_location("lattice1d", "D:\Dropbox\Projects\PyCharmProjects\CellularAutomataMachine\cam\lattice\lattice1d.py")
my_module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(my_module)
lattice = my_module.Lattice1D()
lattice.display()

#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

In this example, we draw text in Russian azbuka.

author: Jan Bodnar
website: zetcode.com 
last edited: September 2015
"""
import random
import sys

import time
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QColor
from PyQt5.QtCore import QThread

from cam.lattice.lattice2d import Lattice2D

class ThreadExample(QThread):
    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def run(self):
        pass

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.lattice = Lattice2D()
        self.lattice.set_rows_columns(256,256)
        self.height = self.width = 1024
        self.r_h = self.height / self.lattice.rows
        self.r_w = self.width / self.lattice.columns
        self.color_white = QColor(255, 255, 255)
        self.color_black = QColor(0, 0, 0)
        self.initUI()

    def initUI(self):
        self.text = "MEH."

        self.setGeometry(300, 300, 512, 512)
        self.setWindowTitle('Draw text')
        self.show()

    def paintEvent(self, event):
        qp = QPainter()
        # self.drawText(event, qp)
        qp.begin(self)
        self.drawRectangles(event, qp)
        qp.end()



    # def drawText(self, event, qp):
    #     qp.setPen(QColor(168, 34, 3))
    #     qp.setFont(QFont('Decorative', 10))
    #     qp.drawText(event.rect(), Qt.AlignCenter, self.text)
    #     # qp.setPen()
    #     qp.setBrush(self.color_black)
    #     qp.drawRect(0, 0, 500, 500)

    def drawRectangles(self, event, qp):
        time_start_update = time.perf_counter()
        self.lattice.update()
        time_update = time.perf_counter() - time_start_update

        time_start_display = time.perf_counter()
        for row in range(self.lattice.rows):
            for col in range(self.lattice.columns):
                color = self.color_white if self.lattice.lattice[row][col] == 0 else self.color_black
                # self.recs[row][col] = self.w.create_rectangle(row * self.r_h, col * self.r_w, (row + 1) * self.r_h, (col + 1) * self.r_w, fill=fill, activefill="red", outline="")
                qp.setPen(color)
                qp.setBrush(color)
                qp.drawRect(row * self.r_h, col * self.r_w, (row + 1) * self.r_h, (col + 1) * self.r_w)

        time_display = time.perf_counter() - time_start_display
        print("update: {} display: {}".format(time_update, time_display))




if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()

    # sys.exit(app.exec_())
    app.exec()
import base64
import math
import random
import time
import tkinter as tk

from cam.lattice.lattice2d import Lattice2D
from cam.lattice.lattice2d_prototyping import CellLattice, Cell
from PIL import Image, ImageTk

class Test:
    def __init__(self):
        self.rows = 512
        self.cols = 512
        self.lattice = CellLattice(self.rows, self.cols)

        self.master = tk.Tk()
        self.height = self.width = 512
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height, highlightthickness=0)
        self.image = self.create_lattice_pbm_image()
        self.image_tag = self.add_image_to_canvas()
        self.canvas.pack(fill=tk.BOTH, expand=tk.YES)
        # self.canvas.pack()
        # self.canvas.after(200, self.update)
        self.master.after(200, self.update)
        # self.canvas.bind("<ButtonPress-1>", self.onCanvasClick)

        self.master.mainloop()

    def add_image_to_canvas(self):
        return self.canvas.create_image(0, 0, image=self.image, anchor=tk.NW)

    def create_lattice_pbm_image(self):
        image = Image.new("RGB", (self.rows, self.cols))

        data = self.generate_screen(self.rows, self.cols)
        image.putdata(data)
        # image = image.resize((self.canvas.winfo_width(), self.canvas.winfo_height() ))
        return ImageTk.PhotoImage(image)

    def generate_screen(self, rows, cols):
        start = time.perf_counter()
        cells = self.lattice.cells
        colors = ((255, 255, 255), (0, 0, 0))
        data = []
        for col in range(cols):
            for row in range(rows):
                data.append(colors[cells[row][col].state])
        print("Generating image data({}x{}): {}".format(rows, cols, time.perf_counter() - start))
        return data

    def update(self):
        # if self.has_canvas_size_changed():
        #     self.create_canvas_rectangles()
        time_start_update = time.perf_counter()
        self.lattice.update()
        time_update = time.perf_counter() - time_start_update

        time_start_display = time.perf_counter()
        self.display()
        time_display = time.perf_counter() - time_start_display

        print(
            "update: {} display image: {}, changes {}, canvas_height: {}, canvas_width: {}".format(time_update,
                                                                                                   time_display,
                                                                                                   len(
                                                                                                       self.lattice.lattice_changes),
                                                                                                   self.canvas.winfo_height(),
                                                                                                   self.canvas.winfo_width()))
        self.master.after(100, func=self.update)

    def display(self):
        self.canvas.delete(self.image_tag)
        self.image = self.create_lattice_pbm_image()
        self.image_tag = self.add_image_to_canvas()

    def has_canvas_size_changed(self):
        return self.width != self.canvas.winfo_width() or self.height != self.canvas.winfo_height()

    # def update_canvas_size(self):
    #     if not self.has_canvas_size_changed():
    #         return
    #     print("Canvas size has changed <{}, {}> -> <{}, {}>".format(self.height, self.width, self.canvas.winfo_height(),
    #                                                                 self.canvas.winfo_width()))
    #     rows = len(self.cells)
    #     cols = len(self.cells[0])
    #     self.height = self.canvas.winfo_height()
    #     self.width = self.canvas.winfo_width()
    #     self.r_h = int(self.height / rows)
    #     self.r_w = int(self.width / cols)
    #     self.canvas.config(width=self.width, height=self.height)
    #     for row in range(rows):
    #         for col in range(cols):
    #             id = self.cells[row][col]
    #             self.set_cell_coords(row, col)

    def unshared_copy(self, inList):
        if isinstance(inList, list):
            return list(map(self.unshared_copy, inList))
        return inList

if __name__ == "__main__":
    test = Test()
    # test.master.mainloop()

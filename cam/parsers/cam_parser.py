import argparse
from typing import List

from cam.parsers.cam_parsers import Parser


class CAMParser(object):
    def __init__(self, is__lattice2d: bool = False):
        # self.parser = argparse.ArgumentParser(description="A simple cellular automata machine.")
        # self.subparsers = self.parser.add_subparsers()
        self.program_parser = self.create_program_parser()
        self.engine_parser = self.create_engine_parser()
        self.lattice1d_parser = self.create_lattice1d_parser(parents=[self.program_parser, self.engine_parser])
        self.lattice2d_parser = self.create_lattice2d_parser(parents=[self.program_parser, self.engine_parser])
        self.set_is_lattice2d(is__lattice2d)

    def parse_input(self, input_arguments):
        # print("Input arguments: " + input_arguments)
        if len(input_arguments.strip()) == 0:
            print("No arguments given, help will be showed:")
            self.print_help_messages()
        split_args = input_arguments.split()
        self.catch_help_argument(split_args)
        namespace = self.create_empty_namespace()
        try:
            namespace, extra = self.engine_parser.parse_known_args(split_args)
            namespace, extra = self.lattice_parser.parse_known_args(extra, namespace=namespace)
            if len(extra) > 0:
                print("Unknown arguments:", extra)
        except TypeError as e:
            # print("Parser error caught!")
            self.print_usage_messages()
        return namespace

    def create_empty_namespace(self):
        input = []
        namespace, input = self.engine_parser.parse_known_args(input)
        namespace, input = self.lattice_parser.parse_known_args(input, namespace=namespace)
        return namespace

    def catch_help_argument(self, args_list: List[str]):
        if "--help" in args_list or "-h" in args_list:
            print("Help argument was caught, help will be shown:")
            args_list[:] = [arg for arg in args_list if arg != "-h" and arg != "--help"]
            self.print_help_messages()

        return args_list

    def print_help_messages(self):
        # self.engine_parser.print_help()
        self.lattice_parser.print_help()

    def print_usage_messages(self):
        # self.engine_parser.print_usage()
        self.lattice_parser.print_usage()

    def set_is_lattice2d(self, is_lattice2d: bool):
        self.lattice_parser = self.lattice2d_parser if is_lattice2d else self.lattice1d_parser

    @staticmethod
    def create_program_parser():
        program_parser = Parser(add_help=False, prog="PROGRAM_PARSER")
        program_parser.description = "Cellular Automata Machine program parser."
        program_parser.add_argument("-q", "--quit", action="store_true", help="will exit the program if set")
        program_parser.add_argument("-l", "--lattice", choices=["1d", "2d"])
        return program_parser

    @staticmethod
    def create_engine_parser():
        engine_parser = Parser(add_help=False, prog="ENGINE_PARSER",
                               description="A simple cellular automata machine engine parser.")
        # engine_parser.add_argument("-q", "--quit", action="store_true", help="will exit the program if set")
        engine_parser.add_argument("-s", "--steps", type=int,
                                   help="number of steps to take, negative number of steps will repeat until paused ")
        engine_parser.add_argument("-d", "--display", type=int,
                                   help="display every n-th step. (default: %(default)s shows all steps)")
        engine_parser.add_argument("--status", action="store_true", help="shows information about the engine")
        engine_running_group = engine_parser.add_mutually_exclusive_group()
        engine_running_group.add_argument("-p", "--pause", action="store_true",
                                          help="If set, engine's pause method will be called")
        engine_running_group.add_argument("-u", "--unpause", action="store_true",
                                          help="If set, engine's unpause method will be called")
        # engine_parser.add_argument("-l", "--lattice", choices=["1d", "2d"])
        engine_parser.add_argument("--show", action="store_true", help="show the current state of the lattice")
        return engine_parser

    @staticmethod
    def create_lattice1d_parser(parents=[]):
        lattice1d_parser = Parser(parents=parents, prog="LATTICE1D_PARSER",
                                  description="Cellular Automata 1D lattice options parser")
        lattice1d_parser.add_argument("-c", "--columns", type=int, help="number of columns in lattice")
        lattice1d_parser.add_argument("--lattice_to_state", type=int,
                                      help="Set all the cells in the lattice to certain state")

        return lattice1d_parser

    @staticmethod
    def create_lattice2d_parser(parents=[]):
        lattice2d_parser = Parser(parents=parents, prog="LATTICE2D_PARSER", description="Cellular Automata 2D lattice options parser")
        lattice2d_parser.add_argument("-r", "--rows", type=int, help="number of rows in lattice")
        lattice2d_parser.add_argument("-c", "--columns", type=int, help="number of columns in lattice")
        lattice2d_parser.add_argument("--lattice_to_state", type=int,
                                      help="Set all the cells in the lattice to certain state")
        return lattice2d_parser


if __name__ == "__main__":
    cam_parser = CAMParser()
    while True:
        args = cam_parser.parse_input(input("input: "))
        print(args)
        # print(args['steps'])
        # print(args.get('steps'))
        print(args.steps)

import sys
import argparse


class Parser(argparse.ArgumentParser):
    # def __init__(self, *args, **kwargs):
    #     super().__init__(self, *args, **kwargs)

    # def parse_known_args(self, arguments, namespace=None):
    #     return self.parser.parse_known_args(arguments, namespace=namespace)

    # def print_help(self):
    #     self.parser.print_help()



    def error(self, message):
        print(message, file=sys.stderr)
        print("'-h' or '--help' for help", file=sys.stderr)
        raise TypeError


class ProgramParser(Parser):
    def __init__(self, add_help=False, parents=[]):
        super().__init__(add_help=add_help, parents=parents, fromfile_prefix_chars="@")
        self.description = "Cellular Automata Machine program parser."
        self.add_argument("-q", "--quit", action="store_true", help="will exit the program if set")
        self.add_argument("-l", "--lattice", choices=["1d", "2d"])
        self.add_argument("-i", "--initial_config", help="Filename to the initial configuration of lattice")
        self.add_argument("-ur", "--update_rule", nargs=2, help="First argument code filename, second argument function name'")
        self.add_argument("--custom_lattice", nargs=2)


class EngineParser(Parser):
    def __init__(self, add_help=False, parents=[]):
        super().__init__(add_help=add_help, parents=parents)
        self.description = "A simple cellular automata machine engine parser."

        # self.add_argument("-q", "--quit", action="store_true", help="will exit the program if set")
        self.add_argument("-s", "--steps", type=int,
                          help="number of steps to take, negative number of steps will repeat until paused ")
        self.add_argument("-d", "--display", type=int,
                          help="display every n-th step. (default: %(default)s shows all steps)")
        self.add_argument("--status", action="store_true", help="shows information about the engine")

        engine_running_group = self.add_mutually_exclusive_group()
        engine_running_group.add_argument("-p", "--pause", action="store_true",
                                          help="If set, engine's pause method will be called")
        engine_running_group.add_argument("-u", "--unpause", action="store_true",
                                          help="If set, engine's unpause method will be called")

        # self.add_argument("-l", "--lattice", choices=["1d", "2d"])
        self.add_argument("--show", action="store_true", help="show the current state of the lattice")


class Lattice1DParser(Parser):
    def __init__(self, parents=[]):
        super().__init__(add_help=True, parents=parents, fromfile_prefix_chars="@")
        self.description = "Cellular Automata 1D lattice options parser"
        self.add_argument("-c", "--columns", type=int, help="number of columns in lattice")
        self.add_argument("--lattice_to_state", type=int,
                          help="Set all the cells in the lattice to certain state")
        self.add_argument("--set_cells", nargs="*", type=int, default=[])


class Lattice2DParser(Parser):
    def __init__(self, parents=[]):
        super().__init__(add_help=True, parents=parents, fromfile_prefix_chars="@")
        self.description = "Cellular Automata 2D lattice options parser"
        self.add_argument("-r", "--rows", type=int, help="number of rows in lattice")
        self.add_argument("-c", "--columns", type=int, help="number of columns in lattice")
        self.add_argument("--lattice_to_state", type=int,
                          help="Set all the cells in the lattice to certain state")
        self.add_argument("--set_cells", nargs="*", type=int, default=[])

from abc import ABCMeta, abstractmethod


class Lattice(metaclass=ABCMeta):
    def __init__(self, update_rule=None):
        self.update_rule = update_rule
        self.lattice = None
        self.buffer_lattice = None
        self.current_step = 0

    @abstractmethod
    def update(self):
        raise NotImplementedError

    def set_lattice(self, lattice):
        self.lattice = lattice
        self.buffer_lattice = self.unshared_copy(self.lattice)
        self.current_step = 0

    def get_lattice(self):
        return self.lattice

    @abstractmethod
    def display(self):
        raise NotImplementedError

    @abstractmethod
    def status(self):
        raise NotImplementedError


    @abstractmethod
    def lattice_to_state(self, state: int):
        raise NotImplementedError

    def unshared_copy(self, inList):
        if isinstance(inList, list):
            return list(map(self.unshared_copy, inList))
        return inList
import ast
import concurrent
import inspect
import random
import types
import concurrent.futures
from threading import Lock

import sys

# import numpy as np
import time

from cam.lattice.lattice import Lattice


class Lattice2D(Lattice):
    def __init__(self):
        super().__init__(self)

        self.rows = 100
        self.columns = 100
        # self.update_rule_ns = {"self": self}
        # self.lock = Lock()

        self.set_lattice(self.generate_random_lattice(self.rows, self.columns))
        self.update_rule = self.default_update_rule_game_of_life_v2
        # self.update_rule = self.update_rule_game_of_life
        # self.display()

    def status(self):
        print("Lattice2D rows: {}, columns: {}, current step: {}".format(self.rows, self.columns, self.current_step))

    def update(self):
        self.lattice_changes = []
        [[self.update_inner(row, col) for col in range(self.columns)] for row in range(self.columns)]

        # for row in range(self.rows):
        #     for column in range(self.columns):
        #         self.update_inner(row, column)

        self.lattice, self.buffer_lattice = self.buffer_lattice, self.lattice
        self.current_step += 1

    def update_inner(self, row, col):
        # print(row, col)
        self.buffer_lattice[row][col] = self.update_rule(row, col)
        if self.lattice[row][col] != self.buffer_lattice[row][col]:
            self.lattice_changes.append((row, col))
            ''

    def update_v2(self):
        self.lattice_changes

    def display(self):
        print("step: " + str(self.current_step))
        for row in self.lattice:
            print("".join(map(str, row)))

    def get_neighbour_state(self, cell_row, cell_column, neighbour_row_offset,
                            neighbour_column_offset: int) -> int:
        neighbour_row = (cell_row + neighbour_row_offset) % self.rows
        neighbour_column = (cell_column + neighbour_column_offset) % self.columns
        return self.lattice[neighbour_row][neighbour_column]

    def get_neighbourhood_state_count(self, row, column, ignore_cell = False):
        radius = 1
        state_count = {}
        for row_offset in range(-radius, radius + 1):
            for column_offset in range(-radius, radius + 1):
                if row_offset == 0 and column_offset == 0:
                    continue
                state = self.get_neighbour_state(row, column, row_offset, column_offset)
                state_count[state] = state_count.get(state, 0) + 1
        return state_count

    def default_update_rule_game_of_life(self, row, col):
        state = self.lattice[row][col]
        neighbours = self.neighbourhood_sum(row, col)
        if state == 0 and neighbours == 3:
            return 1
        if state == 1 and 2 <= neighbours and neighbours <= 3:
            return 1
        return 0

    def default_update_rule_game_of_life_v2(self, row: int, col: int) -> int:
        # state_count = self.get_neighbourhood_state_count(row, col, True)
        state = self.lattice[row][col]
        if row == 0 or row == self.rows - 1 or col == 0 or col == self.columns - 1:
            neighbours = self.get_neighbour_state(row, col, -1, -1) \
                         + self.get_neighbour_state(row, col, -1, 0) \
                         + self.get_neighbour_state(row, col, -1, 1) \
                         + self.get_neighbour_state(row, col, 0, -1) \
                         + self.get_neighbour_state(row, col, 0, 1) \
                         + self.get_neighbour_state(row, col, 1, -1) \
                         + self.get_neighbour_state(row, col, 1, 0) \
                         + self.get_neighbour_state(row, col, 1, 1)
        else:
            neighbours = self.lattice[row - 1][col] \
                         + self.lattice[row + 1][col + 0] \
                         + self.lattice[row + 1][col + 1] \
                         + self.lattice[row + 0][col - 1] \
                         + self.lattice[row + 0][col + 1] \
                         + self.lattice[row + 1][col - 1] \
                         + self.lattice[row + 1][col + 0] \
                         + self.lattice[row + 1][col + 1]

        if state == 0 and neighbours == 3:
            return 1
        if state == 1 and 2 <= neighbours and neighbours <= 3:
            return 1
        return 0

    def set_lattice(self, lattice):
        # with self.lock:
        super().set_lattice(lattice)
        self.rows = len(lattice)
        self.columns = len(lattice[0])
        # print("Updating neighbourhood...", end=" ")
        # self.update_neighbourhood()
        # print("finished")

    def update_neighbourhood(self):
        self.neighbourhood = [[[] for col in range(self.columns)] for row in range(self.rows)]
        for row in range(self.rows):
            for col in range(self.columns):
                self.neighbourhood[row][col] = [
                    ((row - 1) % self.rows, (col - 1) % self.columns),
                    ((row - 1) % self.rows, (col - 0) % self.columns),
                    ((row - 1) % self.rows, (col + 1) % self.columns),
                    ((row - 0) % self.rows, (col - 1) % self.columns),
                    ((row - 0) % self.rows, (col + 1) % self.columns),
                    ((row + 1) % self.rows, (col - 1) % self.columns),
                    ((row + 1) % self.rows, (col - 0) % self.columns),
                    ((row + 1) % self.rows, (col + 1) % self.columns)
                ]

    def neighbourhood_sum(self, row, col):
        n = self.neighbourhood[row][col]
        l = self.lattice
        return l[n[0][0]][n[0][1]] + \
               l[n[1][0]][n[1][1]] + \
               l[n[2][0]][n[2][1]] + \
               l[n[3][0]][n[3][1]] + \
               l[n[4][0]][n[4][1]] + \
               l[n[5][0]][n[5][1]] + \
               l[n[6][0]][n[6][1]] + \
               l[n[7][0]][n[7][1]]

    def set_rows_columns(self, rows: int = None, columns: int = None):
        if rows is None and columns is None:
            return
        rows = rows if rows is not None else self.rows
        columns = columns if columns is not None else self.columns
        if rows == self.rows and columns == self.columns:
            return
        self.set_lattice(self.generate_random_lattice(rows, columns))

    def set_cell(self, row: int, column: int, state: int):
        if row is None or column is None or state is None:
            return
        # with self.lock:
        if 0 <= row < self.rows and 0 <= column <= self.columns:
            self.lattice[row][column] = state

    def get_cell(self, row, column):
        # with self.lock:
        return self.lattice[row][column]

    def set_update_rule(self, new_update_rule):
        if not (inspect.isfunction(new_update_rule) or inspect.ismethod(new_update_rule)):
            print("Update rule should be a function", file=sys.stderr)
            return
        # with self.lock:
        self.update_rule = types.MethodType(new_update_rule, self)

    # def set_update_rule(self, update_rule: str) -> bool:
    #     if not self.is_valid_python(update_rule):
    #         raise ValueError("Given update rule is not valid python code.")
    #     with self.lock:
    #         exec(compile(update_rule, "<string>", "exec"), self.update_rule_ns)
    #         self.update_rule = self.update_rule_ns["update_rule"]
    #     return True

    def lattice_to_state(self, state: int):
        for row in range(self.rows):
            for col in range(self.columns):
                self.lattice[row][col] = state

    @staticmethod
    def generate_random_lattice(rows, columns, states = 2):
        from random import randint
        return [[randint(0, states - 1) for cols in range(columns)] for rows in range(rows)]

    @staticmethod
    def is_valid_python(code):
        try:
            ast.parse(code)
        except SyntaxError as se:
            print(str(se))
            return False

        return True


# class Lattice2DNumpy(Lattice):
#     def __init__(self):
#         super().__init__(self)
#         self.rows = 100
#         self.columns = 100
#         self.states = 2
#         # self.lattice = np.zeros((rows, columns), dtype=np.int8)
#         self.set_lattice(self.generate_random_lattice(self.rows, self.columns, self.states))
#         self.update_rule = self.default_update_rule
#         # print("alive: ", self.lattice.sum())
#
#     def update(self):
#         self.lattice_changes = []
#         it = np.nditer(self.lattice, ["multi_index"])
#         while not it.finished:
#             self.buffer_lattice[it.multi_index] = self.update_rule(self.lattice, it.multi_index[0], it.multi_index[1])
#             if self.lattice[it.multi_index] != self.buffer_lattice[it.multi_index]:
#                 self.lattice_changes.append(it.multi_index)
#             it.iternext()
#         # self.buffer_lattice = self.lattice - self.lattice
#
#         self.lattice, self.buffer_lattice = self.buffer_lattice, self.lattice
#         self.current_step += 1
#         # print("alive: ", self.lattice.sum())
#
#     def default_update_rule(self, l, row, col):
#         # sum = self.get_neighbours(row, col).sum()
#         if row == 0 or row == self.lattice.shape[0] - 1 or col == 0 or col == self.lattice.shape[1] - 1:
#             sum = self.get_neighbour(row - 1, col - 1) \
#                   + self.get_neighbour(row - 1, col) \
#                   + self.get_neighbour(row - 1, col + 1) \
#                   + self.get_neighbour(row, col - 1) \
#                   + self.get_neighbour(row, col + 1) \
#                   + self.get_neighbour(row + 1, col - 1) \
#                   + self.get_neighbour(row + 1, col) \
#                   + self.get_neighbour(row + 1, col + 1)
#         else:
#             sum = self.lattice[row - 1][col] \
#                   + self.lattice[row - 1][col] \
#                   + self.lattice[row - 1][col + 1] \
#                   + self.lattice[row][col - 1] \
#                   + self.lattice[row][col + 1] \
#                   + self.lattice[row + 1][col - 1] \
#                   + self.lattice[row + 1][col] \
#                   + self.lattice[row + 1][col + 1]
#
#         if l[row][col] == 0 and sum == 3:
#             return 1
#         if l[row][col] == 1 and 2 <= sum and sum <= 3:
#             return 1
#         return 0
#
#     def get_neighbour(self, unbounded_row, unbounded_col):
#         return self.lattice[unbounded_row % self.lattice.shape[0]][unbounded_col % self.lattice.shape[1]]
#
#     def get_neighbour_unbounded(self, unbounded_row, unbounded_col):
#         return self.lattice[unbounded_row][unbounded_col]
#
#     def get_neighbours(self, row: int, col: int):
#         neighbourhood = self.lattice.take(range(row - 1, row + 2), mode='wrap', axis=0).take(range(col - 1, col + 2),
#                                                                                              mode='wrap', axis=1)
#         # print(row, col, neighbourhood)
#         return np.delete(neighbourhood, 4)
#
#     def status(self):
#         print("Lattice2D rows: {}, columns: {}, current step: {}".format(self.rows, self.columns, self.current_step))
#
#     def display(self):
#         print("step: " + str(self.current_step))
#         print(self.lattice)
#
#     def set_rows_columns(self, rows: int = None, cols: int = None):
#         # print("Lattice shape {} new shape <{}, {}>".format(self.lattice.shape, rows, cols))
#
#         if rows is None and cols is None:
#             return
#         cur_rows = self.lattice.shape[0]
#         cur_cols = self.lattice.shape[1]
#         rows = rows if rows is not None else cur_rows
#         cols = cols if cols is not None else cur_cols
#         if rows == cur_rows and cols == cur_cols:
#             return
#         self.set_lattice(self.generate_random_lattice(rows, cols))
#         # print("Lattice shape:", self.lattice.shape)
#
#     def set_lattice(self, lattice: np.ndarray):
#         self.lattice = lattice.copy()
#         self.buffer_lattice = lattice.copy()
#         self.rows = lattice.shape[0]
#         self.columns = lattice.shape[1]
#
#     def set_cell(self, row: int, col: int, state: int):
#         self.lattice[row][col] = state
#
#     def get_row_count(self):
#         return self.lattice.shape[0]
#
#     def get_col_count(self):
#         return self.lattice.shape[1]
#
#     def get_cell(self, row: int, col: int):
#         return self.lattice[row][col]
#
#     def set_update_rule(self, new_update_rule):
#         if not (inspect.isfunction(new_update_rule) or inspect.ismethod(new_update_rule)):
#             print("Update rule should be a function", file=sys.stderr)
#             return
#         self.update_rule = types.MethodType(new_update_rule, self)
#
#     def lattice_to_state(self, state: int):
#         self.lattice[...] = state
#
#     @staticmethod
#     def generate_random_lattice(rows: int, cols, states = 2):
#         return np.random.randint(0, states, rows * cols).reshape(rows, cols)
#

class Cell(object):
    def __init__(self, row, col, state = 0):
        self.state = state
        self.row = row
        self.col = col
        self.nw = None
        self.n = None
        self.ne = None
        self.e = None
        self.se = None
        self.s = None
        self.sw = None
        self.w = None
        self.neighbours = [self.nw, self.n, self.ne, self.e, self.se, self.s, self.sw, self.w]
        self.active_neighbours = 0

    def has_active_neighbours(self):
        return self.active_neighbours != 0

    def set_state(self, new_state):
        if self.state != new_state:
            if new_state == 0:
                self.reduce_activity()
            else:
                self.raise_activity()

        self.state = new_state

    def neighbours_sum(self):
        # return self.nw.state + self.n.state + self.ne.state + self.e.state + self.se.state + self.s.state + self.sw.state + self.w.state
        sum = 0
        for n in self.neighbours:
            sum += n.state
        return sum

    def reduce_activity(self):
        for n in self.neighbours:
            n.active_neighbours -= 1

    def raise_activity(self):
        for n in self.neighbours:
            n.active_neighbours += 1

    def set_nw(self, cell):
        self.nw = cell
        self.neighbours[0] = cell
        
    def set_n(self, cell):
        self.n = cell
        self.neighbours[1] = cell

    def set_ne(self, cell):
        self.ne = cell
        self.neighbours[2] = cell

    def set_e(self, cell):
        self.e = cell
        self.neighbours[3] = cell

    def set_se(self, cell):
        self.se = cell
        self.neighbours[4] = cell

    def set_s(self, cell):
        self.s = cell
        self.neighbours[5] = cell

    def set_sw(self, cell):
        self.sw = cell
        self.neighbours[6] = cell

    def set_w(self, cell):
        self.w = cell
        self.neighbours[7] = cell

    def __str__(self):
        return str(self.state)

class CellLattice(object):
    def __init__(self, rows, cols, state=0, test_set=False):
        self.rows = rows
        self.cols = cols
        # self.lattice_list = lattice
        # self.indices = []
        # for row in range(rows):
        #     for col in range(cols):
        #         self.indices.append((row, col))
        self.cells = self.create_cells(self.rows, self.cols)
        self.buffer_cells = self.create_cells(self.rows, self.cols)
        self.connect_cells(self.cells)
        self.connect_cells(self.buffer_cells)
        if test_set:
            self.create_pentadecathlon(10, 10)
            self.create_beacon(7, 30)
            self.create_block(5, 40)
            self.create_glider(5, 50)
            self.create_lwss(30, 30)
        else:
            self.randomize_cells()
        self.update_rule = self.update_rule_game_of_life
        self.lattice_changes = []

    def update(self):
        lattice_changes = []

        buffer_cells = self.buffer_cells
        cells = self.cells
        update_rule = self.update_rule
        # active = 0
        for row in range(self.rows):
            for col in range(self.cols):
        # for indice in self.indices:
        #         row, col = indice
                cell = cells[row][col]
                # if cell.has_active_neighbours():
                #     active += 1
                #     buffer_cell = buffer_cells[row][col]
                #     buffer_cell.set_state(update_rule(cell))
                #     if cell.state != buffer_cell.state:
                #         lattice_changes.append((row, col))
                buffer_cell = buffer_cells[row][col]
                buffer_cell.state = update_rule(cell)
                if cell.state != buffer_cell.state:
                    lattice_changes.append((row, col))
        self.cells, self.buffer_cells = self.buffer_cells, self.cells
        self.lattice_changes = lattice_changes
        # print("How many have active neighbours:", active)

    def update_rule_game_of_life(self, cell):
        sum = cell.neighbours_sum()
        state = cell.state
        if state == 0 and sum == 3:
            return 1
        if state == 1 and 2 <= sum and sum <= 3:
            return 1
        return 0

    def update_rule_switch(self, cell):
        if cell.state == 0:
            return 1
        else:
            return 0

    def update_rule_gol_switch(self, cell):
        sum = cell.neighbours_sum()
        state = cell.state
        if state == 0:
            return 1
        else:
            return 0

    def create_cells(self, rows, cols):
        print("Creating cells...")
        return [[Cell(row, col) for col in range(cols)] for row in range(rows)]

    def connect_cells(self, cells):
        print("Connecting cells...")

        rows = len(cells)
        cols = len(cells[0])
        for row in range(rows):
            for col in range(cols):
                cells[row][col].set_nw(cells[(row - 1) % rows][
                    (col - 1) % cols])
                cells[row][col].set_n(cells[(row - 1) % rows][
                    (col + 0) % cols])
                cells[row][col].set_ne(cells[(row - 1) % rows][
                    (col + 1) % cols])
                cells[row][col].set_e(cells[(row + 0) % rows][
                    (col - 1) % cols])
                cells[row][col].set_se(cells[(row + 0) % rows][
                    (col + 1) % cols])
                cells[row][col].set_s(cells[(row + 1) % rows][
                    (col - 1) % cols])
                cells[row][col].set_sw(cells[(row + 1) % rows][
                    (col + 0) % cols])
                cells[row][col].set_w(cells[(row + 1) % rows][
                    (col + 1) % cols])

    def randomize_cells(self, states=2):
        print("Randomizing cells...")
        cells = self.cells
        for row in range(len(cells)):
            for col in range(len(cells[row])):
                cells[row][col].set_state(random.randint(0, states - 1))

    def set_cell(self, row, col, state):
        self.cells[row][col].set_state(state)

    def set_cells(self, state):
        cells = self.cells
        for row in range(len(cells)):
            for col in range(len(cells[row])):
                cells[row][col].set_state(state)

    def get_bounded_row(self, row, row_offset):
        return (row + row_offset) % self.rows

    def get_bounded_col(self, col, col_offset):
        return (col + col_offset) % self.cols

    def create_glider(self, row, col):
        self.cells[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 1)].state = 1

    def create_lwss(self, row, col):
        self.cells[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, -2)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 2)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, -2)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 2)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 0)].state = 1

    def create_beacon(self, row, col):
        self.cells[self.get_bounded_row(row, -1)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 2)].state = 1
        self.cells[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 2)].state = 1

    def create_pentadecathlon(self, row, col):
        self.cells[self.get_bounded_row(row, -4)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, -3)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, -2)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, -2)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 3)][self.get_bounded_col(col, -1)].state = 1
        self.cells[self.get_bounded_row(row, 3)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 4)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 5)][self.get_bounded_col(col, 0)].state = 1

    def create_block(self, row, col):
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 1)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)].state = 1
        self.cells[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 1)].state = 1


if __name__ == "__main__":
    lattice = CellLattice(512, 512, test_set=False);
    while True:
        time_start_update = time.perf_counter()
        lattice.update()
        time_update = time.perf_counter() - time_start_update
        print("update: {}".format(time_update))
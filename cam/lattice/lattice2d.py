import ast
import concurrent
import inspect
import types
import concurrent.futures
from threading import Lock


import sys


from cam.lattice.lattice import Lattice


class Lattice2D(Lattice):
    def __init__(self):
        super().__init__(self)

        self.rows = 512
        self.columns = 512
        # self.lock = Lock()

        # self.set_lattice(self.generate_random_lattice(self.rows, self.columns, 1))
        self.set_lattice(self.generate_empty_lattice(self.rows, self.columns))
        self.create_pentadecathlon(10, 10)
        self.create_beacon(7, 30)
        self.create_block(5, 40)
        self.create_glider(5, 50)
        self.create_lwss(30, 30)
        self.update_rule = self.default_update_rule_game_of_life

    def status(self):
        print("Lattice2D rows: {}, columns: {}, current step: {}".format(self.rows, self.columns, self.current_step))

    def update(self):
        self.lattice_changes = []
        # [[self.update_inner(row, col) for col in range(self.columns)] for row in range(self.columns)]

        for row in range(self.rows):
            for column in range(self.columns):
                self.update_inner(row, column)

        self.lattice, self.buffer_lattice = self.buffer_lattice, self.lattice
        self.current_step += 1

    def update_inner(self, row, col):
        self.buffer_lattice[row][col] = self.update_rule(row, col)
        if self.lattice[row][col] != self.buffer_lattice[row][col]:
            self.lattice_changes.append((row, col))

    def default_update_rule_game_of_life(self, row: int, col: int) -> int:
        state = self.lattice[row][col]
        if row == 0 or row == self.rows - 1 or col == 0 or col == self.columns - 1:
            # neighbours = self.get_neighbour_state(row, col, -1, -1) \
            #              + self.get_neighbour_state(row, col, -1, 0) \
            #              + self.get_neighbour_state(row, col, -1, 1) \
            #              + self.get_neighbour_state(row, col, 0, -1) \
            #              + self.get_neighbour_state(row, col, 0, 1) \
            #              + self.get_neighbour_state(row, col, 1, -1) \
            #              + self.get_neighbour_state(row, col, 1, 0) \
            #              + self.get_neighbour_state(row, col, 1, 1)
            neighbours = self.lattice[(row - 1) % self.rows][(col - 1) % self.columns] \
            + self.lattice[(row - 1) % self.rows][(col + 0) % self.columns] \
            + self.lattice[(row - 1) % self.rows][(col + 1) % self.columns] \
            + self.lattice[(row + 0) % self.rows][(col - 1) % self.columns] \
            + self.lattice[(row + 0) % self.rows][(col + 1) % self.columns] \
            + self.lattice[(row + 1) % self.rows][(col - 1) % self.columns] \
            + self.lattice[(row + 1) % self.rows][(col + 0) % self.columns] \
            + self.lattice[(row + 1) % self.rows][(col + 1) % self.columns]
        else:
            neighbours = self.lattice[row - 1][col - 1] \
                         + self.lattice[row - 1][col + 0] \
                         + self.lattice[row - 1][col + 1] \
                         + self.lattice[row + 0][col - 1] \
                         + self.lattice[row + 0][col + 1] \
                         + self.lattice[row + 1][col - 1] \
                         + self.lattice[row + 1][col + 0] \
                         + self.lattice[row + 1][col + 1]

        if state == 0 and neighbours == 3:
            return 1
        if state == 1 and 2 <= neighbours and neighbours <= 3:
            return 1
        return 0

    def display(self):
        print("step: " + str(self.current_step))
        for row in self.lattice:
            print("".join(map(str, row)))

    def get_neighbour_state(self, cell_row: int, cell_column: int, neighbour_row_offset: int,
                            neighbour_column_offset: int) -> int:
        neighbour_row = (cell_row + neighbour_row_offset) % self.rows
        neighbour_column = (cell_column + neighbour_column_offset) % self.columns
        return self.lattice[neighbour_row][neighbour_column]

    def get_neighbour_index(self, row, col, row_offset, col_offset):
        return (row + row_offset) % self.rows, (col + col_offset) % self.columns

    def get_bounded_row(self, row, row_offset):
        return (row + row_offset) % self.rows

    def get_bounded_col(self, col, col_offset):
        return (col + col_offset) % self.columns

    #     self.neighbourhood = [[[] for col in range(self.columns)] for row in range(self.rows)]
# def update_neighbourhood(self):
    #     for row in range(self.rows):
    #         for col in range(self.columns):
    #             self.neighbourhood[row][col] = [
    #                 ((row - 1) % self.rows, (col - 1) % self.columns),
    #                 ((row - 1) % self.rows, (col - 0) % self.columns),
    #                 ((row - 1) % self.rows, (col + 1) % self.columns),
    #                 ((row - 0) % self.rows, (col - 1) % self.columns),
    #                 ((row - 0) % self.rows, (col + 1) % self.columns),
    #                 ((row + 1) % self.rows, (col - 1) % self.columns),
    #                 ((row + 1) % self.rows, (col - 0) % self.columns),
    #                 ((row + 1) % self.rows, (col + 1) % self.columns)
    #             ]
    #
    # def neighbourhood_sum(self, row, col):
    #     n = self.neighbourhood[row][col]
    #     l = self.lattice
    #     return l[n[0][0]][n[0][1]] + \
    #            l[n[1][0]][n[1][1]] + \
    #            l[n[2][0]][n[2][1]] + \
    #            l[n[3][0]][n[3][1]] + \
    #            l[n[4][0]][n[4][1]] + \
    #            l[n[5][0]][n[5][1]] + \
    #            l[n[6][0]][n[6][1]] + \
    #            l[n[7][0]][n[7][1]]
    def create_glider(self, row, col):
        self.lattice[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 1)] = 1

    def create_lwss(self, row, col):
        self.lattice[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, -2)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 2)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, -2)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 2)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 0)] = 1

    def create_beacon(self, row, col):
        self.lattice[self.get_bounded_row(row, -1)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 2)] = 1
        self.lattice[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 2)] = 1

    def create_pentadecathlon(self, row, col):
        self.lattice[self.get_bounded_row(row, -4)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, -3)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, -2)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, -2)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, -1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 2)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 3)][self.get_bounded_col(col, -1)] = 1
        self.lattice[self.get_bounded_row(row, 3)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 4)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 5)][self.get_bounded_col(col, 0)] = 1

    def create_block(self, row, col):
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 0)][self.get_bounded_col(col, 1)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 0)] = 1
        self.lattice[self.get_bounded_row(row, 1)][self.get_bounded_col(col, 1)] = 1

    def set_rows_columns(self, rows: int = None, columns: int = None):
        if rows is None and columns is None:
            return
        rows = rows if rows is not None else self.rows
        columns = columns if columns is not None else self.columns
        if rows == self.rows and columns == self.columns:
            return
        self.set_lattice(self.generate_random_lattice(rows, columns))

    def set_cell(self, row, column, state):
        if row is None or column is None or state is None:
            return
        if 0 <= row < self.rows and 0 <= column <= self.columns:
            self.lattice[row][column] = state

    def get_cell(self, row, column):
        return self.lattice[row][column]

    def set_lattice(self, lattice):
        if all(len(i) == len(lattice[0]) for i in lattice):
            super().set_lattice(lattice)
            self.rows = len(lattice)
            self.columns = len(lattice[0])
        else:
            print("New lattice wasn't used because all rows weren't the same length", file=sys.stderr)



    def set_update_rule(self, new_update_rule):
        if not (inspect.isfunction(new_update_rule) or inspect.ismethod(new_update_rule)):
            print("Update rule should be a function", file=sys.stderr)
            return
        self.update_rule = types.MethodType(new_update_rule, self)

    def lattice_to_state(self, state: int):
        for row in range(self.rows):
            for col in range(self.columns):
                self.lattice[row][col] = state

    @staticmethod
    def generate_random_lattice(rows: int, columns: int, states: int = 2):
        from random import randint
        return [[randint(0, states - 1) for cols in range(columns)] for rows in range(rows)]

    @staticmethod
    def generate_empty_lattice(rows: int, columns: int):
        return [[0 for cols in range(columns)] for rows in range(rows)]

    @staticmethod
    def is_valid_python(code: str) -> bool:
        try:
            ast.parse(code)
        except SyntaxError as se:
            print(str(se))
            return False
        return True

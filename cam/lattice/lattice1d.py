import ast
import inspect
import types
from threading import Lock

from typing import List, Dict, Callable, Type, Any

import sys

from cam.lattice.lattice import Lattice


class Lattice1D(Lattice):
    # wolfram_rule_30 = """def update_rule(col: int) -> int:
    #     neighbourhood = self.get_neighbourhood(col)
    #     if neighbourhood in {100, 11, 10, 1}:
    #         return 1
    #     else:
    #         return 0"""

    def __init__(self, columns: int = 512, update_rule: str = None, initial_lattice: List[int] = None):
        super().__init__(self)
        self.columns = columns
        self.lock = Lock()
        if initial_lattice is not None:
            self.set_lattice(initial_lattice)
        else:
            self.set_lattice(self.generate_empty_lattice(self.columns))

        if update_rule is not None:
            self.set_update_rule(update_rule)
        else:
            self.update_rule = self.default_update_rule

    def status(self):
        print("Lattice1D: columns: {}, current step: {}".format(self.columns,self.current_step))

    def update(self):
        with self.lock:
            for column in range(self.columns):
                self.buffer_lattice[column] = self.update_rule(column)
            self.lattice, self.buffer_lattice = self.buffer_lattice, self.lattice
            self.current_step += 1

    def display(self):
        with self.lock:
            print("".join(map(str, self.lattice)), end=" | ")
            print("step: " + str(self.current_step))

    def get_neighbourhood_state_count(self, column: int, radius: int = 1, ignore_cell: bool = False) -> Dict[int, int]:
        with self.lock:
            if not self.check_radius_in_bounds(radius):
                raise ValueError("Given radius not in lattice bounds [0, {}]".format(self.columns / 2))
            # radius %= self.columns

            state_count = {}
            for neighbour_offset in range(-radius, radius + 1):
                if ignore_cell and neighbour_offset == 0:
                    continue
                state = self.lattice[column + neighbour_offset]
                state_count[state] = state_count.setdefault(state, 0) + 1
            return state_count

    def get_neighbourhood(self, column: int, radius: int = 1):
        if not self.check_radius_in_bounds(radius):
            raise ValueError("Given radius not in lattice bounds [0, {}]".format(self.columns / 2))

        power = radius * 2
        neighbourhood = 0

        for neighbour_offset in range(-radius, radius + 1):
            neighbour_column = (column + neighbour_offset) % self.columns
            state = self.lattice[neighbour_column]
            neighbourhood += state * 10 ** power
            power -= 1
        return neighbourhood

    # def set_update_rule(self, update_rule: str) -> bool:
    #     if not self.is_valid_python(update_rule):
    #         raise ValueError("Given update rule is not valid python code.")
    #     exec(compile(update_rule, "<string>", "exec"), self.update_rule_ns)
    #     self.update_rule = self.update_rule_ns["update_rule"]
    #     return True

    def set_update_rule(self, update_rule: Callable[[int], int]):
        # print(type(update_rule))
        if inspect.isfunction(update_rule):
            self.update_rule = types.MethodType(update_rule, self)
            print(type(self.update_rule))
        else:
            print("Update rule should be a function", file=sys.stderr)

    def default_update_rule(self, col: int) -> int:
        neighbourhood = self.get_neighbourhood(col)
        if neighbourhood in {100, 11, 10, 1}:
            return 1
        else:
            return 0

    def set_lattice(self, lattice: List[int]):
        if not all(isinstance(cell, int) for cell in lattice):
            print("1D lattice should be a List[int]", file=sys.stderr)
            return
        super().set_lattice(lattice)
        self.columns = len(self.lattice)

    def get_lattice(self):
        return self.lattice

    def set_cell(self, column: int, state: int):
        if column is None or state is None:
            return
        self.lattice[column] = state
        self.current_step = 0

    def get_cell(self, column: int) -> int:
        return self.lattice[column]

    def check_radius_in_bounds(self, radius: int) -> bool:
        return 0 <= radius * 2 < self.columns

    def set_columns(self, columns: int) -> object:
        if columns is None or columns < 10 or columns == self.columns:
            return
        self.set_lattice(self.generate_empty_lattice(columns))

    def lattice_to_state(self, state: int):
        if state is None:
            return
        with self.lock:
            for column in range(self.columns):
                self.lattice[column] = state

    @staticmethod
    def generate_empty_lattice(columns: int, states: int = 2) -> List[int]:
        from random import randint
        return [randint(0, states-1) for n in range(columns)]
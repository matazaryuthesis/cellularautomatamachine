from cam.lattice.lattice2d import Lattice2D


class Lattice2DTriangle(Lattice2D):
    pass


class Lattice2DSquare(Lattice2D):
    pass


class Lattice2DHexagonal(Lattice2D):
    pass

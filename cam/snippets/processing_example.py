from multiprocessing import Process

def f(name):
    print ('hello', name)
    name = "yolo"

if __name__ == '__main__':
    name = "bob"
    p = Process(target=f, args=(name,))
    print(name)
    p.start()
    p.join()
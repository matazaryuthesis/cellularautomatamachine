import random

import numpy as np
import time

from cam.lattice.lattice2d_prototyping import Cell


# class Test(object):
#     def __init__(self, x):
#         self.x = x
#         self.buffer_x = x
#
#     def set_x(self, new_x):
#         self.x = new_x
#
#     def switch(self):
#         self.x, self.buffer_x = self.buffer_x, self.x


class Cell(object):
    def __init__(self, row, col, state = 0):
        self.state: int = state
        self.buffer_state: int = state
        self.row = row
        self.col = col
        self.nw = None
        self.n = None
        self.ne = None
        self.e = None
        self.se = None
        self.s = None
        self.sw = None
        self.w = None
        self.neighbours = [self.nw, self.n, self.ne, self.e, self.se, self.s, self.sw, self.w]
        self.active_neighbours = 0

    def switch(self):
        self.state, self.buffer_state = self.buffer_state, self.state

    def has_active_neighbours(self):
        return self.active_neighbours != 0

    def set_state(self, new_state):
        if self.state != new_state:
            if new_state == 0:
                self.reduce_activity()
            else:
                self.raise_activity()

        self.state = new_state

    def neighbours_sum(self):
        # return self.nw.state + self.n.state + self.ne.state + self.e.state + self.se.state + self.s.state + self.sw.state + self.w.state
        sum = 0
        for n in self.neighbours:
            sum += n.state
        return sum

    def reduce_activity(self):
        for n in self.neighbours:
            n.active_neighbours -= 1

    def raise_activity(self):
        for n in self.neighbours:
            n.active_neighbours += 1

    def set_nw(self, cell):
        self.nw = cell
        self.neighbours[0] = cell

    def set_n(self, cell):
        self.n = cell
        self.neighbours[1] = cell

    def set_ne(self, cell):
        self.ne = cell
        self.neighbours[2] = cell

    def set_e(self, cell):
        self.e = cell
        self.neighbours[3] = cell

    def set_se(self, cell):
        self.se = cell
        self.neighbours[4] = cell

    def set_s(self, cell):
        self.s = cell
        self.neighbours[5] = cell

    def set_sw(self, cell):
        self.sw = cell
        self.neighbours[6] = cell

    def set_w(self, cell):
        self.w = cell
        self.neighbours[7] = cell

    def __str__(self):
        return str(self.state)


def update_rule(cell):
    sum = 0
    for n in cell.neighbours:
        sum += n.state
    state = cell.state
    if state == 0 and sum == 3:
        cell.buffer_state = 1
    elif state == 1 and 2 <= sum and sum <= 3:
        cell.buffer_state = 1
    else:
        cell.buffer_state = 0
    return cell


def create_cells(rows, cols):
    print("Creating cells...")
    return [[Cell(row, col) for col in range(cols)] for row in range(rows)]


def connect_cells(cells):
    print("Connecting cells...")
    for row in range(rows):
        for col in range(cols):
            cells[row][col].set_nw(cells[(row - 1) % rows][
                                       (col - 1) % cols])
            cells[row][col].set_n(cells[(row - 1) % rows][
                                      (col + 0) % cols])
            cells[row][col].set_ne(cells[(row - 1) % rows][
                                       (col + 1) % cols])
            cells[row][col].set_e(cells[(row + 0) % rows][
                                      (col - 1) % cols])
            cells[row][col].set_se(cells[(row + 0) % rows][
                                       (col + 1) % cols])
            cells[row][col].set_s(cells[(row + 1) % rows][
                                      (col - 1) % cols])
            cells[row][col].set_sw(cells[(row + 1) % rows][
                                       (col + 0) % cols])
            cells[row][col].set_w(cells[(row + 1) % rows][
                                      (col + 1) % cols])


def switch(cell):
    cell.state, cell.buffer_state = cell.buffer_state, cell.state
    return cell

def print_lattice():
    for row in range(rows):
        for col in range(cols):
            print(lattice[row][col], end=" ")
        print()
    print()

def update():
    for row in range(rows):
        for col in range(cols):
            cell = lattice[row][col]
            sum = 0
            for n in cell.neighbours:
                sum += n.state
            state = cell.state
            if state == 0 and sum == 3:
                cell.buffer_state = 1
            elif state == 1 and 2 <= sum and sum <= 3:
                cell.buffer_state = 1
            else:
                cell.buffer_state = 0

def switch_all():
    for row in range(rows):
        for col in range(cols):
            cell = lattice[row][col]
            cell.state, cell.buffer_state = cell.buffer_state, cell.state

if __name__ == "__main__":
    v_switch = np.vectorize(switch)
    v_update_rule = np.vectorize(update_rule)
    rows, cols = 512, 512
    lattice = np.array([[Cell(row, col, random.randint(1, 1)) for col in range(cols)] for row in range(rows)], dtype=object)
    connect_cells(lattice)
    # print_lattice()
    for step in range(100):
        start = time.perf_counter()
        # update()
        # switch_all()
        lattice = v_update_rule(lattice)
        lattice = v_switch(lattice)
        total = time.perf_counter() - start
        print("Time: ", total)
        # print_lattice()






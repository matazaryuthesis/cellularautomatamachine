import tkinter as tk
from random import Random


class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("A simple GUI")
        self.height = 200
        self.width = 200
        self.canvas = tk.Canvas(master, width=self.width, height=self.height)
        self.canvas.pack()

        self.draw_rectangle()
        self.create_lattice()



    def draw_rectangle(self):
        self.rect = self.canvas.create_rectangle(0, 0, 50, 50, fill="red")

    def create_lattice(self):
        self.lattice = [[Random.randint(0, 1) for col in range(self.width)] for row in range(self.height)]

    def create_lattice_pixmap(self):
        pass


if __name__ == "__main__":
    root = tk.Tk()
    my_gui = MyFirstGUI(root)
    root.mainloop()
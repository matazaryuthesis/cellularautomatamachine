from cam.lattice.lattice1d import Lattice1D
from cam.parsers.cam_parsers import Parser


class Lattice1DHandler(object):
    def __init__(self, lattice_1d: Lattice1D, lattice_1d_parser: Parser):
        if lattice_1d is None or lattice_1d_parser is None:
            raise TypeError



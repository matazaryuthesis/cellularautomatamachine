import argparse

from cam.cam_engine import CAMEngine
from cam.lattice.lattice1d import Lattice1D
from cam.lattice.lattice2d import Lattice2D
from cam.parsers.cam_parser import CAMParser
from cam.parsers.cam_parsers import EngineParser, Lattice2DParser, Lattice1DParser


class CAM:
    def __init__(self, arguments):
        self.exit = False
        self.parser = CAMParser()

        self.lattice = Lattice1D()
        # self.lattice.set_cell(4, 1)
        # self.parse_input(arguments)

        self.cam_engine = CAMEngine(self, self.lattice)
        self.cam_engine.start()

    def main_loop(self):
        try:
            while not self.exit:
                try:
                    user_input = self.get_input()
                    self.parse_input(user_input)
                except KeyboardInterrupt:
                    self.cam_engine.pause()
                    print("Keyboard Interrupt caught!")
        finally:
            self.status()
            self.terminate()
            print("PROGRAM STOPPED")

    def get_input(self) -> str:
        return input()  # "'exit' || '<number>' (negative for infinite) of steps: ")

    def parse_input(self, user_input: str):
        user_args = self.parser.parse_input(user_input)
        self.input_handler(user_args)
        # if user_input == "":
        #     self.cam_engine.toggle_pause()
        # elif user_input == "exit":
        #     self.terminate()
        # elif user_input == "pause":
        #     self.cam_engine.pause()
        # elif user_input == "unpause":
        #     self.cam_engine.unpause()
        # elif user_input == "status":
        #     self.status()
        # elif match("^-?\d+$", str(user_input)):
        #     self.cam_engine.set_steps(int(user_input))

    def input_handler(self, args):
        if issubclass(self.cam_engine.lattice.__class__, Lattice1D):
            self.handle_lattice1d_options(args)
        else:
            self.handle_lattice2d_options(args)
        self.handle_engine_options(args)

    def handle_engine_options(self, args: argparse.Namespace):
        if args.status:
            self.status()
        if args.quit:
            self.terminate()
        if args.steps is not None:
            self.cam_engine.set_steps(args.steps)
        if args.display is not None:
            self.cam_engine.set_display_nth_step(args.display)
        self.update_lattice_type(args.lattice)
        if args.show:
            self.cam_engine.lattice.display()
        if args.pause:
            self.cam_engine.pause()
        if args.unpause:
            self.cam_engine.unpause()

    def handle_lattice1d_options(self, args):

        self.cam_engine.lattice.set_columns(args.columns)
        self.cam_engine.lattice.lattice_to_state(args.lattice_to_state)

    def handle_lattice2d_options(self, args):
        self.cam_engine.lattice.set_rows_columns(args.rows, args.columns)
        if args.lattice_to_state is not None:
            self.cam_engine.lattice.lattice_to_state(args.lattice_to_state)

    def update_lattice_type(self, lattice_type: str = None):
        if lattice_type == '1d':
            self.cam_engine.set_lattice(Lattice1D())
            self.parser.set_is_lattice2d(False)
        if lattice_type == '2d':
            self.cam_engine.set_lattice(Lattice2D())
            self.parser.set_is_lattice2d(True)

    def status(self):
        self.cam_engine.status()

    def start(self):
        self.main_loop()

    def display(self):
        self.lattice.display()

    # def update(self):
    #     self.lattice.update()

    def terminate(self):
        self.exit = True
        self.cam_engine.terminate()


if __name__ == "__main__":
    import sys

    # print(sys.argv)
    cam = CAM(sys.argv)
    cam.start()

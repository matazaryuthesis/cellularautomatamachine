import math
import time
import tkinter as tk

from cam.lattice.lattice2d_prototyping import Lattice2DNumpy


class Test:
    def __init__(self):
        self.lattice = Lattice2DNumpy()
        self.lattice.set_rows_columns(512, 512)

        self.master = tk.Tk()
        self.height = self.width = 512
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height)

        self.r_h = self.height / self.lattice.get_row_count()
        self.r_w = self.width / self.lattice.get_col_count()
        self.recs = self.lattice.lattice.tolist()
        self.create_canvas_rectangles()

        self.canvas.pack(fill=tk.BOTH, expand=tk.YES)

        self.canvas.after(200, self.update)

        # self.master.mainloop()


    # def get_bounded_row(self, row, offset):
    #     return (row + offset) % self.lattice.rows
    #
    # def get_bounded_column(self, column, offset):
    #     return (column + offset) % self.lattice.columns

    def create_canvas_rectangles(self):
        # self.r_h = self.w.winfo_height() / self.lattice.rows
        # self.r_w = self.w.winfo_width() / self.lattice.columns
        print("rows {} cols {}".format(self.lattice.get_row_count(), self.lattice.get_col_count()))
        for row in range(self.lattice.get_row_count()):
            for col in range(self.lattice.get_col_count()):
                fill = "white" if self.lattice.get_cell(row, col) == 0 else "black"
                self.recs[row][col] = self.canvas.create_rectangle(row * self.r_h, col * self.r_w, (row + 1) * self.r_h,
                                                                   (col + 1) * self.r_w, fill=fill, activefill="red",
                                                                   outline="")
                self.canvas.tag_bind(self.recs[row][col], "<ButtonPress-1>", self.onCellClick)

    def onCellClick(self, event):
        row = math.floor(event.y / self.r_h)
        col = math.floor(event.x / self.r_w)
        state = self.lattice.get_cell(row, col)
        print("width: {}, height: {}, row: {}, col: {}, state: {}".format(event.x, event.y, row, col, state))
        rect = event.widget.find_closest(event.x, event.y)

        # print("fill: {}".format(event.widget.find_withtag(rect)))

    def update(self):
        time_start_update = time.perf_counter()
        self.lattice.update()
        time_update = time.perf_counter() - time_start_update

        time_start_display = time.perf_counter()
        self.display()
        time_display = time.perf_counter() - time_start_display
        print("update: {} display: {}, canvas_height: {}, canvas_width: {}".format(time_update, time_display,
                                                                                   self.canvas.winfo_height(),
                                                                                   self.canvas.winfo_width()))
        self.canvas.after(200, func=self.update)

    def display(self):
        # for row in range(self.lattice.get_row_count()):
        #     for col in range(self.lattice.get_col_count()):
        #         fill = "white" if self.lattice.get_cell(row, col) == 0 else "black"
        #         self.canvas.itemconfig(self.recs[row][col], fill=fill)
        changes = self.lattice.lattice_changes
        for i in range(len(self.lattice.lattice_changes)):
                fill = "white" if self.lattice.lattice[changes[i][0]][changes[i][1]] == 0 else "black"
                self.canvas.itemconfig(self.recs[changes[i][0]][changes[i][1]], fill=fill)

    def has_canvas_size_changed(self):
        return self.r_w != self.canvas.winfo_width() or self.r_h != self.canvas.winfo_height()

    def unshared_copy(self, inList):
        if isinstance(inList, list):
            return list(map(self.unshared_copy, inList))
        return inList


if __name__ == "__main__":
    test = Test()
    test.master.mainloop()

import math
import time
import tkinter as tk
from typing import List

from cam.lattice.lattice2d import Lattice2D
from cam.lattice.lattice2d_prototyping import CellLattice, Cell


class Test:
    def __init__(self):
        self.lattice = CellLattice(512, 512, test_set=False)

        self.master = tk.Tk()
        self.height = self.width = 1024
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height, highlightthickness=0)

        self.cells = self.create_canvas_rectangles(self.lattice.cells)

        self.canvas.pack(fill=tk.BOTH, expand=tk.YES)
        self.canvas.pack()
        self.canvas.after(200, self.update)
        self.canvas.bind("<ButtonPress-1>", self.onCellClick)

        self.master.mainloop()

    def create_canvas_rectangles(self, lattice_cells: List[List[Cell]]):
        cells = [[lattice_cells[row][col].state for col in range(len(lattice_cells[row]))] for row in
                 range(len(lattice_cells))]
        self.r_h = int(self.height / len(cells))
        self.r_w = int(self.width / len(cells[0]))
        for row in range(len(cells)):
            for col in range(len(cells[row])):
                fill = "white" if cells[row][col] == 0 else "black"
                cells[row][col] = self.canvas.create_rectangle(0, 0, 0, 0, fill=fill, activefill="red",
                                                               outline="")
                self.set_cell_coords(cells, row, col)
        return cells

    def set_cell_coords(self, cells, row, col):
        self.canvas.coords(cells[row][col], row * self.r_h, col * self.r_w, (row + 1) * self.r_h,
                           (col + 1) * self.r_w)

    def onCellClick(self, event):
        row = math.floor(event.y / self.r_h)
        col = math.floor(event.x / self.r_w)
        state = self.lattice.cells[col][row].state
        print("width: {}, height: {}, row: {}, col: {}, state: {}".format(event.x, event.y, row, col, state))
        # rect = event.widget.find_closest(event.x, event.y)
        # print("fill: {}".format(event.widget.find_withtag(rect)))

    def test_pbm_creation(self):
        pbm = "P1\n# Test Lattice\n{} {}\n".format(self.lattice.cols, self.lattice.rows)
        for row in range(self.lattice.rows):
            # for col in range(self.lattice.cols):
            pbm += " ".join(map(str, self.lattice.cells[row])) + "\n"
        return pbm

    def update(self):
        # if self.has_canvas_size_changed():
        #     self.create_canvas_rectangles()
        time_start_update = time.perf_counter()
        self.lattice.update()
        time_update = time.perf_counter() - time_start_update

        # time_pbm_start = time.perf_counter()
        # image = self.test_pbm_creation()
        # time_pbm = time.perf_counter() - time_pbm_start
        # print("Image creation:", time_pbm)
        time_start_display = time.perf_counter()
        self.display()
        time_display = time.perf_counter() - time_start_display

        print(
            "update: {} display: {}, changes {}, canvas_height: {}, canvas_width: {}".format(time_update, time_display,
                                                                                             len(
                                                                                                 self.lattice.lattice_changes),
                                                                                             self.canvas.winfo_height(),
                                                                                             self.canvas.winfo_width()))
        self.canvas.after(100, func=self.update)

    def display(self):
        # self.update_canvas_size()
        changes = self.lattice.lattice_changes
        for i in range(len(changes)):
            fill = "white" if self.lattice.cells[changes[i][0]][changes[i][1]].state == 0 else "black"
            self.canvas.itemconfig(self.cells[changes[i][0]][changes[i][1]], fill=fill)

    def has_canvas_size_changed(self):
        return self.width != self.canvas.winfo_width() or self.height != self.canvas.winfo_height()

    def update_canvas_size(self):
        if not self.has_canvas_size_changed():
            return
        print("Canvas size has changed <{}, {}> -> <{}, {}>".format(self.height, self.width, self.canvas.winfo_height(),
                                                                    self.canvas.winfo_width()))
        rows = len(self.cells)
        cols = len(self.cells[0])
        self.height = self.canvas.winfo_height()
        self.width = self.canvas.winfo_width()
        self.r_h = int(self.height / rows)
        self.r_w = int(self.width / cols)
        self.canvas.config(width=self.width, height=self.height)
        for row in range(rows):
            for col in range(cols):
                id = self.cells[row][col]
                self.set_cell_coords(row, col)

    def unshared_copy(self, inList):
        if isinstance(inList, list):
            return list(map(self.unshared_copy, inList))
        return inList


if __name__ == "__main__":
    test = Test()
    # test.master.mainloop()

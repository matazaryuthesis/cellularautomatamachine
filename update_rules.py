def wolfram_rule_30(self, col):
    neighbourhood = self.get_neighbourhood(col)
    if neighbourhood in {100, 11, 10, 1}:
        return 1
    else:
        return 0


def always_zero_1d(self, col):
    return 0

def always_zero_2d(self, row, col):
    return 0

def game_of_life(self, row, column):
	state_count = self.get_neighbourhood_state_count(row=row,column=column, ignore_cell=True)
	if self.lattice[row][column] == 1:
	    return 1 if 2 <= state_count.get(1, 0) <= 3 else 0
	else:
	    return 1 if state_count.get(1, 0) == 3 else 0
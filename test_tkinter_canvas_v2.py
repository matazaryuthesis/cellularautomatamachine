import math
import time
import tkinter as tk

from cam.lattice.lattice2d import Lattice2D


class Test:
    def __init__(self):
        self.lattice = Lattice2D()
        self.lattice.set_rows_columns(512, 512)
        self.master = tk.Tk()
        self.height = self.width = 512
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height, highlightthickness=0)
        # self.float_ratio = float(self.lattice.rows) / self.lattice.columns
        self.r_h = self.height / self.lattice.rows
        self.r_w = self.width / self.lattice.columns
        self.cells = self.unshared_copy(self.lattice.lattice)
        self.create_canvas_rectangles()
        self.canvas.pack(fill=tk.BOTH, expand=tk.YES)
        self.canvas.pack()
        self.canvas.after(200, self.update)

        self.master.mainloop()

    def get_bounded_row(self, row, offset):
        return (row + offset) % self.lattice.rows

    def get_bounded_column(self, column, offset):
        return (column + offset) % self.lattice.columns

    def create_canvas_rectangles(self):
        # self.r_h = self.w.winfo_height() / self.lattice.rows
        # self.r_w = self.w.winfo_width() / self.lattice.columns
        self.canvas.bind("<ButtonPress-1>", self.onCellClick)
        for row in range(self.lattice.rows):
            for col in range(self.lattice.columns):
                fill = "white" if self.lattice.lattice[row][col] == 0 else "black"
                self.cells[row][col] = self.canvas.create_rectangle(0,0,0,0 , fill=fill, activefill="red",
                                                                    outline="")
                self.set_cell_coords(row, col)

    def set_cell_coords(self, row, col):
        self.canvas.coords(self.cells[row][col], row * self.r_h, col * self.r_w, (row + 1) * self.r_h,(col + 1) * self.r_w)

    def onCellClick(self, event):
        row = math.floor(event.y / self.r_h)
        col = math.floor(event.x / self.r_w)
        state = self.lattice.get_cell(col, row)
        print("width: {}, height: {}, row: {}, col: {}, state: {}".format(event.x, event.y, row, col, state))
        rect = event.widget.find_closest(event.x, event.y)

        # print("fill: {}".format(event.widget.find_withtag(rect)))

    def update(self):
        # if self.has_canvas_size_changed():
        #     self.create_canvas_rectangles()
        time_start_update = time.perf_counter()
        self.lattice.update()
        time_update = time.perf_counter() - time_start_update

        time_start_display = time.perf_counter()
        self.display()
        time_display = time.perf_counter() - time_start_display
        print("update: {} display: {}, canvas_height: {}, canvas_width: {}".format(time_update, time_display,
                                                                                   self.canvas.winfo_height(),
                                                                                   self.canvas.winfo_width()))
        self.canvas.after(100, func=self.update)

    def display(self):
        self.update_canvas_size()
        changes = self.lattice.lattice_changes
        for i in range(len(self.lattice.lattice_changes)):
            fill = "white" if self.lattice.lattice[changes[i][0]][changes[i][1]] == 0 else "black"
            self.canvas.itemconfig(self.cells[changes[i][0]][changes[i][1]], fill=fill)

    def has_canvas_size_changed(self):
        return self.width != self.canvas.winfo_width() or self.height != self.canvas.winfo_height()

    def update_canvas_size(self):
        if not self.has_canvas_size_changed():
            return
        print("Canvas size has changed <{}, {}> -> <{}, {}>".format(self.height, self.width, self.canvas.winfo_height(),
                                                                    self.canvas.winfo_width()))
        rows = len(self.cells)
        cols = len(self.cells[0])
        self.height = self.canvas.winfo_height()
        self.width = self.canvas.winfo_width()
        self.r_h = self.height / rows
        self.r_w = self.width / cols
        self.canvas.config(width=self.width, height=self.height)
        for row in range(rows):
            for col in range(cols):
                id = self.cells[row][col]
                self.set_cell_coords(row, col)

    def unshared_copy(self, inList):
        if isinstance(inList, list):
            return list(map(self.unshared_copy, inList))
        return inList


if __name__ == "__main__":
    test = Test()
    # test.master.mainloop()

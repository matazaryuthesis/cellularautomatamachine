import sys, pygame, time

from cam.lattice.lattice2d_prototyping import CellLattice


def display():
    # screen.fill(color=colors[1])
    for row in range(rows):
        for col in range(cols):
            state = cells[row][col].state
            # if state != 0:
            screen.fill(colors[state], rects[row][col])
    screen.blit(getLabel("Step " + str(step), 16, (255, 0, 0)), (0, 0))
    pygame.display.flip()


def how_long(func, name):
    start = time.perf_counter()
    func()
    end = time.perf_counter()
    print("Step: {}, name: {}, time: {}".format(step, name, end - start))

def getLabel(text, size, color):
    default_font = pygame.font.get_default_font()
    font_renderer = pygame.font.Font(default_font, size)
    return font_renderer.render(text, 1, color)

if __name__ == "__main__":
    pygame.init()
    pygame.font.init()
    rows, cols = 512, 512
    size = width, height = 512, 512

    lattice = CellLattice(height, width)
    cells = lattice.cells
    screen: pygame.Surface = pygame.display.set_mode(size)
    colors = ((255, 255, 255), (0, 0, 0))

    # rects = [[pygame.Rect(row * int(height / rows), col * int(width / cols), (row + 1) * int(height / rows),
    #                       (col + 1) * int(width / cols)) for col in
    #           range(cols)] for row in
    #          range(rows)]
    rects = [[pygame.Rect(row, col, (row + 1),
                          (col + 1)) for col in
              range(cols)] for row in
             range(rows)]
    step = 0


    while True:
        step += 1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        pygame.display.set_caption("Step " + str(step))
        how_long(display, "Display")
        how_long(lattice.update, "Update")

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': "Bachelor's thesis project. A simple GUI program for cellular automata.",
    'author': "Lukas Pertel",
    "url": "https://bitbucket.org/MatazaRyu/cellularautomatamachine",
    "download_url": "https://bitbucket.org/MatazaRyu/cellularautomatamachine/get/master.zip",
    "author_email": "lukas.pertel@ttu.ee",
    "version": "0.1",
    "install_requires": [],
    "packages": ["cam"],
    "scripts": [],
    "name": "CAM"
}

setup(**config)
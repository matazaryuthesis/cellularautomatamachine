import time
import tkinter as tk

from cam.lattice.lattice2d_prototyping import CellLattice


class Test:
    def __init__(self, name, write_img = False):
        self.rows = 512
        self.cols = 512
        self.lattice = CellLattice(self.rows, self.cols)

        self.step = 0
        self.name = name
        self.write_img = write_img

        self.master = tk.Tk()
        self.height = self.width = 512
        self.canvas = tk.Canvas(self.master, width=self.width, height=self.height, highlightthickness=0)
        self.image = None
        self.image_tag = None
        self.display()
        self.canvas.pack(fill=tk.BOTH, expand=tk.YES)
        # self.canvas.pack()
        # self.canvas.after(200, self.update)
        self.master.after(200, self.update)
        # self.canvas.bind("<ButtonPress-1>", self.onCanvasClick)

        self.master.mainloop()

    def add_image_to_canvas(self):
        return self.canvas.create_image(0, 0, image=self.image, anchor=tk.NW)

    def create_lattice_pbm_image(self):
        data = self.generate_ppm_p6(self.rows, self.cols)
        image = tk.PhotoImage(data=data)
        # if self.write_img:
        #     self.write_img_to_file(data)
        x_zoom = int(self.canvas.winfo_width() / self.rows) + 1
        y_zoom = int(self.canvas.winfo_height() / self.cols) + 1
        image = image.zoom(x_zoom, y_zoom)
        return image

    def write_img_to_file(self, img):
        img_name = "img/{}_{}.ppm".format(self.name, self.step)
        file = open(img_name, "wb+")
        file.write(img)
        file.close()
        print("Image {} saved".format(img_name))

    def generate_ppm_p6(self, rows, cols, comment="No comment"):
        def generate_screen(width, height):
            for pixel in range(width * height):
                yield pixel % 255
                yield pixel * 2 % 255
                yield pixel * 3 % 255

        def generate_screen_v2(cells, rows, cols):
            colors = ((255, 255, 255), (0, 0, 0))
            # screen = [colors[]) for pixel in range(width * height)]
            screen = []
            for col in range(cols):
                for row in range(rows):
                    screen.extend(colors[cells[row][col].state])
            return screen
            # return [rgb for pixel in screen for rgb in pixel]

        start = time.perf_counter()

        b_header = bytes("P6\n#{}\n{} {}\n255\n".format(comment, rows, cols), "utf8")
        b_screen = bytes(generate_screen_v2(self.lattice.cells, rows, cols))
        print("Generating PPM P6 ({}x{}): {}".format(rows, cols, time.perf_counter() - start))
        return b_header + b_screen

    def update(self):
        # if self.has_canvas_size_changed():
        #     self.create_canvas_rectangles()
        self.step += 1
        time_start_update = time.perf_counter()
        self.lattice.update()
        time_update = time.perf_counter() - time_start_update

        time_start_display = time.perf_counter()
        self.display()
        time_display = time.perf_counter() - time_start_display

        print(
            "update: {} display image: {}, changes {}, canvas_height: {}, canvas_width: {}".format(time_update, time_display,
                                                                                             len(
                                                                                                 self.lattice.lattice_changes),
                                                                                             self.canvas.winfo_height(),
                                                                                             self.canvas.winfo_width()))
        self.master.after(100, func=self.update)

    def display(self):
        self.canvas.delete(self.image_tag)
        self.image = self.create_lattice_pbm_image()
        self.image_tag = self.add_image_to_canvas()


    def has_canvas_size_changed(self):
        return self.width != self.canvas.winfo_width() or self.height != self.canvas.winfo_height()

    # def update_canvas_size(self):
    #     if not self.has_canvas_size_changed():
    #         return
    #     print("Canvas size has changed <{}, {}> -> <{}, {}>".format(self.height, self.width, self.canvas.winfo_height(),
    #                                                                 self.canvas.winfo_width()))
    #     rows = len(self.cells)
    #     cols = len(self.cells[0])
    #     self.height = self.canvas.winfo_height()
    #     self.width = self.canvas.winfo_width()
    #     self.r_h = int(self.height / rows)
    #     self.r_w = int(self.width / cols)
    #     self.canvas.config(width=self.width, height=self.height)
    #     for row in range(rows):
    #         for col in range(cols):
    #             id = self.cells[row][col]
    #             self.set_cell_coords(row, col)

    def unshared_copy(self, inList):
        if isinstance(inList, list):
            return list(map(self.unshared_copy, inList))
        return inList


if __name__ == "__main__":
    test = Test("test")
    # test.master.mainloop()
